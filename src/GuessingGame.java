/**
 * Murach, J. ( 2011). Murach’s Java Programming, Training and Reference, 4th
 * Edition, Fresno, CA: Mike Murach & Associates. Inc. 
 * Modifications by N.See, 2013
 */

import java.util.Scanner;
import java.util.*;

public class GuessingGame {

    public static void main(String[] args) {
      
        SeeHeading.getHeading("Assignment Four");
        // display a welcome message
        System.out.println("  Welcome to the Guess the Number Game.\n");
        // display operational messages
       
        // create the Scanner object
        Scanner sc = new Scanner(System.in);
        
        String choice = "y";
        while (choice.equalsIgnoreCase("y")) {
            
                int numberToGuess = (int) (Math.random() * 99);  // from 0 to 99
                numberToGuess++; // from 1 to 100
                int guess = 0;
                int counter = 1;
                int difference = numberToGuess - guess;
                
                System.out.println("\n\n  I'm thinking of a number from 1 to 100.");
                System.out.println("  Try to guess it.\n");
                
                while (guess != numberToGuess) {
                guess = getIntWithinRange(sc,"  Enter number -> ", 0, 101);
               
            if (guess == numberToGuess) {
                System.out.println("  You got it in " + counter + " tries."); 
                if (counter <=  3) 
                System.out.println("  Great work! You are a mathematical wizard. \n");
                else if (counter > 3 && counter <= 7) 
                System.out.println("  Not too bad! You've got some potential. \n");
                else if (counter > 7)
                System.out.println("  What took you so long? Maybe you should take some lessons. \n");
            }
            else if (guess > numberToGuess)
                {
                    if (difference > 10)
                    {
                        System.out.println("  Wowz! Way too high! Guess again.\n");
                    }
                    else
                    {
                        System.out.println("  Too high! Guess again.\n");
                    }
                }
                else if (guess < numberToGuess)
                {
                    System.out.println("  Woops! Too low! Guess again.\n");
                }
                counter++;
            }
             // see if the user wants to continue

            // use validation method to get a valid string
            choice = getChoiceString(sc, "  Play another round? (y/n): ", "y", "n");
            System.out.println();
        }
            System.out.println("\n\n  Program ended. Come back and visit soon!");
            SeeDate.printfDate();
            SeeDate.printfTime();
                       
    } // end main
    public static int getIntWithinRange(Scanner sc, String prompt,
    int min, int max)
    {
        int i = 0;
        boolean isValid = false;
        while (!isValid)
        {
            try {
               
            i = getInt(sc, prompt);
            if (i <= min) 
                System.out.println(
                    "  Error! Number must be greater than " + min + ".\n");
            else if (i >= max)
                System.out.println(
                    "  Error! Number must be less than " + max + ".\n");
            else 
               isValid = true;
            } // end try
            catch (Exception e) {
                System.out.printf("\n  *  Program failed within the getIntWithinRange method\n");    
                e.printStackTrace(); 
            } // end catch
        } // end while
      
        return i;
    }
    
    public static int getInt(Scanner sc, String prompt)
    {
        int i = 0;
        boolean isValid = false;
        while (isValid == false)
        {
            System.out.print(prompt);
            if (sc.hasNextInt())
            {
                i = sc.nextInt();
                isValid = true;
            }
            else
            {
                System.out.println("  Error! Invalid entry. Try again.");
            }
            sc.nextLine();  // discard any other data entered on the line  
        }
        return i;
    }
  //add a new method that forces the user to enter a string
    private static String getRequiredString(Scanner sc, String prompt)
    {
        String s = "";
        boolean isValid = false;
        while (isValid == false)
        {
            System.out.print(prompt);
            s = sc.nextLine();
            if (s.equals(""))
            {
                System.out.println("  Error! This entry is required. Try again.");
            }
            else
            {
                isValid = true;
            }
        }
        return s;
    }

    // add another new method that forces the user to enter one of two strings
    private static String getChoiceString(Scanner sc, String prompt, String s1, String s2)
    {
        String s = "";
        boolean isValid = false;
        while (isValid == false)
        {
            s = getRequiredString(sc, prompt);
            if (
                !s.equalsIgnoreCase(s1) &&
                !s.equalsIgnoreCase(s2)
                )
            {
                System.out.println("  Error! Entry must be '"+
                    s1 +"' or '"+ s2 +"'. Try again.");
            }
            else
            {
                isValid = true;
            }
        }
        return s;
    }
}